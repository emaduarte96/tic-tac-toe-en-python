﻿from random import randrange

pizarra = [[1,2,3],[4,'X',6],[7,8,9]]


#
# la función acepta un parámetro el cual contiene el estado actual del tablero
# y lo muestra en la consola
#
def DisplayBoard(board):
    print('+','+','+','+', sep='-------')
    print('|','|','|','|', sep='       ')
    print('|',board[0][0],'|',board[0][1],'|',board[0][2],'|', sep='   ')
    print('|','|','|','|', sep='       ')
    print('+','+','+','+', sep='-------')
    print('|','|','|','|', sep='       ')
    print('|',board[1][0],'|',board[1][1],'|',board[1][2],'|', sep='   ')
    print('|','|','|','|', sep='       ')
    print('+','+','+','+', sep='-------')
    print('|','|','|','|', sep='       ')
    print('|',board[2][0],'|',board[2][1],'|',board[2][2],'|', sep='   ')
    print('|','|','|','|', sep='       ')
    print('+','+','+','+', sep='-------')
    
#
# la función EnterMove acepta el estado actual del tablero y pregunta al usuario acerca de su movimiento, 
# verifica la entrada y actualiza el tablero acorde a la decisión del usuario
#

def EnterMove(board):
    f = input('Ingresa tu movimiento: ')
    
    for i in range(len(board)):
        for j in range(len(board[i])):
            if board[i][j] == int(f) and board[i][j] != 'X':
                board[i][j] = 'O'
                break
    
    return board
        


# la función MakeListOfFreeFields examina el tablero y construye una lista de todos los cuadros vacíos 
# la lista esta compuesta por tuplas, cada tupla es un par de números que indican la fila y columna
#
def MakeListOfFreeFields(board):
    tup = ()
    list = []
    
    for i in range(len(board)):
        for j in range(len(board[i])):
            if board[i][j] != 'X' and  board[i][j] != 'O':
                tup = (i,j)
                list.append(tup)
    return list
                

# la función analiza el estatus del tablero para verificar si
# el jugador que utiliza las 'O's o las 'X's ha ganado el juego
#
def VictoryFor(board, sign):
        estado = 1
        msg = 0
    
        if board[0][0] == 'X' and board[0][1] == 'X' and board[0][2] == 'X':
            estado = 2
        elif board[0][0] == 'O' and board[0][1] == 'O' and board[0][2] == 'O':
            estado = 3
        elif board[1][0] == 'X' and board[1][1] == 'X' and board[1][2] == 'X':
            estado = 2
        elif board[2][0] == 'X' and board[2][1] == 'X' and board[2][2] == 'X':
            estado = 2
        elif board[2][0] == 'O' and board[2][1] == 'O' and board[2][2] == 'O':
            estado = 3
        elif board[0][0] == 'X' and board[1][0] == 'X' and board[2][0] == 'X':
            estado = 2
        elif board[0][0] == 'O' and board[1][0] == 'O' and board[2][0] == 'O':
            estado = 3
        elif board[0][1] == 'X' and board[1][1] == 'X' and board[2][1] == 'X':
            estado = 2
        elif board[0][2] == 'X' and board[2][1] == 'X' and board[2][2] == 'X':
            estado = 2
        elif board[0][2] == 'O' and board[1][2] == 'O' and board[2][2] == 'O':
            estado = 3
        elif len(sign) == 2:
            estado = 4
                
        return estado      
                

 # la función DrawMove dibuja el movimiento de la maquina y actualiza el tablero
#

def DrawMove(board):
    auxArray = []
    
    for i in range(10):
        auxArray = randrange(8)
        
    
    
    for i in range(len(board)):
        for j in range(len(board[i])):
            if board[i][j] == auxArray and board[i][j] != 'O':
                board[i][j] = 'X'
                break
    
    return board



# la función initGame arranca el juego
#
def initGame(arg):
    cicloDeVida = 1
    
    inicio = input('Quiere comenzar a jugar? Responda (responda si o no): ')
    
    if inicio == 'si':
        DisplayBoard(pizarra)
        while cicloDeVida == 1:
            DisplayBoard(EnterMove(pizarra))
            DisplayBoard(DrawMove(pizarra))
            MakeListOfFreeFields(DrawMove(pizarra))
            cicloDeVida = VictoryFor(DrawMove(pizarra), MakeListOfFreeFields(pizarra))
            
    if(cicloDeVida == 2):
      print('¡La consola gana el juego!')
    elif cicloDeVida == 3:
      print('¡El jugador gana el juego!')
    elif cicloDeVida == 4:
      print('El juego termino en un aburrido empate')

            
            
            
    elif inicio == 'no':
        print('No hay problema ¡ya habra tiempo para jugar!')
        return
    else:
        print('ERROR: Ingreso una respuesta incorrecta')
        return
    
    
        
initGame(pizarra)